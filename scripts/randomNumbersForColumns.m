function outputMat = randomNumbersForColumns(xlsFileName,sheetNumber,numberOfRandMSBs)
% read excel sheet
% generate random numbers for different columns

% structure of the excel sheet
% columns:
%     1: msb id
%     2: sliceId (z)
%     7: symmetric
%     8: asymmetric

% xlsFileName = '/home/thanuja/Dropbox/PROJECTS/MSB/annotations/xls/s108_msb_mss.xls';
% sheetNumber = 1;
% numberOfRandMSBs = 5;

outputMat = zeros(numberOfRandMSBs,3);

[num,heads] = xlsread(xlsFileName,sheetNumber);

if(isnan(num(1,1)))
    num(1,:) = [];
end

% MSBS : col1
randMsbIDs = getVector(num,1,numberOfRandMSBs);
outputMat(:,1) = randMsbIDs;

% symmetric MSBs : col7
randSymMsbIDs = getVector(num,7,numberOfRandMSBs);
outputMat(:,2) = randSymMsbIDs;

% asymmetric MSBs : col8
randasymMsbIDs = getVector(num,8,numberOfRandMSBs);
outputMat(:,3) = randasymMsbIDs;


function vector = getVector(msbMatrix, colID,numberOfRandMSBs)
vectIDs = msbMatrix(:,colID);
msbIDs = msbMatrix(vectIDs>0,1);
vector = getRandNumsFromVector(msbIDs,numberOfRandMSBs);

function randNumbers = getRandNumsFromVector(inputVector,numRand)

numElements = numel(inputVector);

randIndices = ceil(rand(numRand,1).*numElements);
randNumbers = inputVector(randIndices);

