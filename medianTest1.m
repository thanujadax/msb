xlsfilename = '/home/thanuja/Dropbox/PROJECTS/MSB/Leila/synapsePerBouton.xls';
sheetID = 1;
alpha = 0.017;

[num,txt] = xlsread(xlsfilename,sheetID);
[p_signrank,h] = signrank(num(:,1),num(:,2));

[p_ranksum,h_ranksum,stats_ranksum] = ranksum(num(:,1),num(:,2),alpha);