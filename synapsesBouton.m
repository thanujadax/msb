% synapse per bouton

xlsFileName = '/home/thanuja/Dropbox/PROJECTS/MSB/Leila/SynapsesBouton.xlsx';
sheetID = 2;

[num,heads] = xlsread(xlsFileName,sheetID);

meanSynapsesGroup = zeros(3,5);
sdSynapsesGroup = zeros(3,5);

for i=1:5
    % iso
    meanSynapsesGroup(1,i) = mean(num((1:4),i));
    sdSynapsesGroup(1,i) = std(num((1:4),i));
    
    % VL
    meanSynapsesGroup(2,i) = mean(num((5:8),i));
    sdSynapsesGroup(2,i) = std(num((5:8),i));
    
    % LT
    meanSynapsesGroup(3,i) = mean(num((9:10),i));
    sdSynapsesGroup(3,i) = std(num((9:10),i));
    
    % FT
    meanSynapsesGroup(4,i) = mean(num((11:14),i));
    sdSynapsesGroup(4,i) = std(num((11:14),i));
end