% Kruskal Wallis test
% returns the p-value for the null hypothesis
% that the data in each column of the matrix x comes from the same distribution
% for 3 or more samples
% nonparametric
% unpaired


% xlsFileName = '/home/thanuja/Dropbox/PROJECTS/MSB/annotations/synapsePerBouton_all.xls';
xlsFileName = '/home/thanuja/Dropbox/PROJECTS/myelin/myelinSummary.xlsx'
xlsSheet = 1;

% cols = [1 2 3]; % columns whose median is tested
col1 = 1;
maxRows = 4;
numCols = 3;

data = zeros(maxRows,numCols);
[num,txt] = xlsread(xlsFileName,xlsSheet);
data(:,1) = num(1:4,col1);
data(:,2) = num(5:8,col1);
data(:,3) = num(9:12,col1);

p = kruskalwallis(data)