function stats1 = groupAnalysis()

% xlsFileName = '/home/thanuja/Dropbox/PROJECTS/MSB/annotations/synapsePerBouton_all.xls';
% xlsFileName = '/home/thanuja/Dropbox/PROJECTS/MSB/Leila/FullDataSummary.xls'

xlsFileName = '/home/thanuja/Dropbox/PROJECTS/myelin/myelinSummary.xlsx'
sheet = 1;

[num,heads] = xlsread(xlsFileName,sheet);

% msb number = col1

% vol = col 11;


% col1, msb #; col2:vol, col3: msb density
% ISO

col1 = 1; % 

stats = zeros(3,4);



volData = zeros(4,1);
volData(:) = num(1:4,col1);

stats1(1,1) = mean(volData(:)); % mean
stats1(1,2) = std(volData(:));  % sd
stats1(1,3) = median(volData(:)); % median
stats1(1,4) = std(volData(:)) / sqrt(length(volData)); %SEM


% VL
volData = zeros(4,1);
volData(:)=num(5:8,col1);

stats1(2,1) = mean(volData(:));
stats1(2,2) = std(volData(:));
stats1(2,3) = median(volData(:));
stats1(2,4) = std(volData(:)) / sqrt(length(volData));



% FT
volData = zeros(4,1);
volData(:)=num(9:12,col1);

stats1(3,1) = mean(volData(:));
stats1(3,2) = std(volData(:));
stats1(3,3) = median(volData(:));
stats1(3,4) = std(volData(:)) / sqrt(length(volData));