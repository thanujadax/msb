% Two sided Wilcoxon rank sum test
% Equivalent Mann-Whitney U test. Tests for equal medians
% for 2 samples
% nonparametric
% unpaired


xlsFileName = '/home/thanuja/Dropbox/PROJECTS/MSB/annotations/synapsePerBouton_all.xls';
xlsSheet = 1;
col1 = 24;
col2 = 24;
maxRows = 4;

[num,txt] = xlsread(xlsFileName,xlsSheet);
data1 = num(7:8,col1);
data2 = num(5:6,col2);
p = ranksum(data1,data2)

boxplot([data1 data2])