% sub volume plots

xlsFileName = '/home/thanuja/Dropbox/PROJECTS/MSB/subVolAnalysis.xlsx';
outputPath = '/home/thanuja/Dropbox/ETH/thesisMain/images/msb/subvolumes';

[num,str] = xlsread(xlsFileName);

for i=1:4
figure;
bar(num(i,:),'g')
set(gca,'FontSize',16);
titleStr = str{(i+1),1};
xLabel = 'sub-volume ID';
yLabel = 'MSB count';
title(titleStr,'FontSize',26);
xlabel(xLabel,'FontSize',22);
ylabel(yLabel,'FontSize',22);

set(gcf, 'PaperPosition', [0 0 5 5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5 5]); %Set the paper to have width 5 and height 5.

outputFileName = sprintf('%s.svg',titleStr);
outputFileName = fullfile(outputPath,outputFileName);
print(outputFileName,'-dsvg');

end