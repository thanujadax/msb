% msb subvolume analysis

inputExcelSheet = '/home/thanuja/Dropbox/PROJECTS/MSB/Leila/xls2/s1503_msb.xlsx';
outputPath = '/home/thanuja/projects/data/MSB/subVolAnalysis/s1503';

% inputExcelSheet = '/home/thanuja/Dropbox/PROJECTS/MSB/Leila/complete excelsheets/sample1503full.xlsx';

sheet  = 1;

xRes = 5; % nm
yRes = 5; % nm
zRes = 10; % nm

xSize = 1600;
ySize = 1600;
zSize = 822;

numSubVols = 4;

subVolSize = floor(zSize/4);

volume = xSize*xRes * ySize*yRes * zSize*zRes / (1000^3); % microm^3
subVolVol = volume/numSubVols;

[num,headers] = xlsread(inputExcelSheet);

% remove first row if NaN

if(isnan(num(1,1)))
    num(1,:) = [];
end

numMsbPerSubVol = zeros(numSubVols,1);
msbDensityPerSubVol = zeros(numSubVols,1);

stopInd = 0;
for i=1:numSubVols
    startInd = stopInd + 1;
    stopInd = startInd + subVolSize -1;
    numMsbPerSubVol(i) = sum(num(:,2)>startInd & num(:,2)<=stopInd);
    msbDensityPerSubVol(i) = numMsbPerSubVol(i) / subVolVol;
end


MSBdensityMean = mean(msbDensityPerSubVol);
MSBdensitySd = std(msbDensityPerSubVol);
MSBdensityMedian = median(msbDensityPerSubVol);

statVect = zeros(1,3);
statVect(1) = MSBdensityMean;
statVect(2) = MSBdensitySd;
statVect(3) = MSBdensityMedian;

% save outpouts
msbPerSV_fileName = fullfile(outputPath,'msbPerVol.txt');
save(msbPerSV_fileName,'numMsbPerSubVol','-ASCII');